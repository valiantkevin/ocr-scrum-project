package com.example.tesseractoncf.model.io.request;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

@Data
@Entity
public class SplitBill {
    @Id
    private BigInteger id;

    private String nama_tertagih;

    private BigInteger hp_tertagih;

    private String nama_penagih;

    private BigInteger hp_penagih;

    private String nama_item;

    private Integer qty;

    private Integer price;

    private Integer diskon;

    private Date tgl_transaksi;
}
