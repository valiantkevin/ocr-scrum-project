package com.example.tesseractoncf.services;

import java.awt.image.BufferedImage;

import com.example.tesseractoncf.dao.SplitBillDao;
import com.example.tesseractoncf.model.io.request.SplitBill;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.imageio.ImageIO;
import org.springframework.stereotype.Service;

@Service
@RequestScope
public class OcrService {

    private static final Logger log = LoggerFactory.getLogger(OcrService.class);
    private final tesseract.TessBaseAPI api;
    private final TmpFile tmpFile;
    private static final String ALPHA_NUMERIC_STRING = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";

    @Autowired
    SplitBillDao splitBillDao;

    public OcrService(@Value("${tessdata.dir:classpath:/tessdata}") File tessdata,
            @Value("${tessdata.lang:eng}") String lang,
            TmpFile tmpFile) {
        this.tmpFile = tmpFile;
        this.api = new tesseract.TessBaseAPI();
        String path = tessdata.toPath().toString();
        log.debug("Load tessdata from {} (lang={}).", path, lang);
        if (this.api.Init(path, lang) != 0) {
            throw new IllegalStateException("Could not initialize tesseract.");
        }
    }

    public String read(MultipartFile multipartFile) {
        File tmp = this.tmpFile.write(multipartFile).asFile();
        return this.read(tmp);
    }

    public String read(File file) {
        String total = "";
        BytePointer outText = null;
        lept.PIX image = null;
        try {
            image = lept.pixRead(file.getAbsolutePath());
            this.api.SetImage(image);
            // Get OCR result
            outText = this.api.GetUTF8Text();
            String result = outText.getString().toLowerCase();
            String lines[] = result.split("\\r?\\n");

            for (int i = 0; i < lines.length; i++) {
                if (lines[i].contains("tot")) {
                    total = "";
                    String[] splitTotal = lines[i].split("tot");
                    char[] words = splitTotal[1].toCharArray();
                    for (char word : words) {
                        if (Character.isDigit(word)) {
                            total = total + word;
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
            file.delete();

//            return total;
            return outText == null ? "" : outText.getString();
        } finally {
            if (outText != null) {
                outText.deallocate();
            }
            if (image != null) {
                lept.pixDestroy(image);
            }
        }
    }

    @PreDestroy
    void cleanup() {
        log.debug("Cleanup TessBaseAPI.");
        this.api.End();
    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public String getTotal(String path) throws MalformedURLException, IOException {
        URL url = new URL(path);
        BufferedImage img = ImageIO.read(url);

        String fileName = randomAlphaNumeric(20);

        File outputfile = new File(fileName + ".png");
        ImageIO.write(img, "png", outputfile);

        return this.read(outputfile);
    }

    public Object saveSplitBill(List<SplitBill> splitBillList) {
        for (SplitBill splitBill : splitBillList)
            System.out.println("save");
            //splitBillDao.save(splitBill);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}
