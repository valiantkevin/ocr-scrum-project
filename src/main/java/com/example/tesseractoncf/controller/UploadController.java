package com.example.tesseractoncf.controller;

import java.io.IOException;
import java.util.List;

import com.example.tesseractoncf.model.io.request.SplitBill;
import com.example.tesseractoncf.model.io.request.Url;
import com.example.tesseractoncf.services.OcrService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UploadController {

    @Autowired
    private OcrService ocr;

    @PostMapping("/total")
    public String total(@RequestBody Url url) throws IOException {
        String result = ocr.getTotal(url.getUrl());
        return result;
    }

    @GetMapping("/save")
    public Object save(@RequestBody List<SplitBill> splitBillList) {
        return this.ocr.saveSplitBill(splitBillList);
    }
}
